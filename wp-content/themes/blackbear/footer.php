<div class="footer-container">
    <footer class="clearfix">
    <div class="tier-content-block">
    	<div class="footer-center">
	    	<!-- logo -->
			<div class="logo-footer">
				<a href="<?php echo home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/style/images/BBL-logo-light.png" alt="Logo" class="logo-footer">
				</a>
			</div>
			<!-- /logo -->
			<h2 class="yellow">Discover a New Path</h2>
			<a href="/admissions/"><button class="btn btn-green icon-pinecone-sm-drk">Start Admissions</button></a><br clear="all"><br />
            Accredited by:<br />
            <div class="footer-accreditations">
            <a href="https://www.jointcommission.org/about_us/about_the_joint_commission_main.aspx"><img src="/wp-content/uploads/joint-commission-logo.png" align="center" style="vertical-align: top;" alt="The Joint Commission: Accreditation non-profit for health care organizations and programs"></a>
<div style="display: inline-block; height: 63px; padding-top: 10px;"><a id="bbblink" class="sehzbas" href="https://www.bbb.org/atlanta/business-reviews/drug-abuse-and-addiction-info-and-treatment/black-bear-lodge-in-sautee-nacoochee-ga-27696400?addressid=1#bbbseal" title="Black Bear Rehab, Drug Abuse & Addiction  Info & Treatment, Sautee Nacoochee, GA" style="display: inline-block;position: relative;overflow: hidden; width: 100px; height: 63px; margin: 0px; padding: 0px;"><img style="padding: 0px; border: none;" id="bbblinkimg" src="http://seal-nashville.bbb.org/logo/sehzbas/foundations-recovery-network-37038606.png" width="200" height="38" alt="Black Bear Rehab, Drug Abuse & Addiction  Info & Treatment, Sautee Nacoochee, GA" /></a><script type="text/javascript">var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Ffoundations-recovery-network-37038606.js') + "' type='text/javascript'%3E%3C/script%3E"));</script></div>
<div style="display: inline-block"><script src="https://static.legitscript.com/seals/3417924.js"></script></div>
<img style="display: inline-block; width: 79px;" class="naatp-logo" alt="NAATP "src="<?php bloginfo('stylesheet_directory'); ?>/style/images/accreditations/naatp-stacked-white.svg">
            </div>
    	</div><!-- end footer-center -->
    	
    	<div class="footer-side-left">
    		<ul class="facility-info">
    			<li class="icon-sm-phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer (Discover Path)"]'); ?></li>
    			<li class="icon-sm-email"><a href="/contact/">Email Us</a></li>
				<li class="icon-sm-users"><a href="/about/">About Us</a></li>
				<li class="icon-pinecone-lrg-lt"><a href="https://jobs.uhsinc.com/frn-residential-treatment-careers/jobs?page=1&brand=Black+Bear+Lodge">Careers</a></li>
    			
    		</ul>
		<ul class="facility-address">
			<li class="icon-sm-location"><a href="https://www.google.com/maps/place/Black+Bear+Lodge/@34.6583268,-83.7221458,16z/data=!4m2!3m1!1s0x885f485504f17c83:0x124b3e62316d2e35" target="_blank">310 Black Bear Ridge<br />Sautee Nacoochee, GA 30571<br /><span style="text-decoration:underline;">Map it</span></a></li>
		</ul>
    	</div><!-- end footer-side -->
    	
		<div class="footer-side-right">
    		<ul class="footer-social">
    			<li class="icon-sm-facebook"><a target="_blank" href="https://www.facebook.com/BlackBearRecovery">Facebook</a></li>
    			<li class="icon-sm-twitter"><a target="_blank" href="https://twitter.com/BlackBearRehab">Twitter</a></li>
				<li class="icon-sm-linkedin"><a target="_blank" href="https://www.linkedin.com/company/black-bear-lodge-treatment-center/">LinkedIn</a></li>
    		</ul>
    	</div><!-- end footer-side -->
    </div><!-- end tier-content-block-->
    </footer>
</div><!-- end footer-container -->
<div id="bottom-bar">
	<div id="bottom-bar-left">
		<div class="bottom-c">
		    <div class="message-c">
		    	<?php if(get_post_meta($post->ID,'ranklab_footer_rotator_large', true)) { ?>
		    		<h2><?php echo get_post_meta($post->ID,'ranklab_footer_rotator_large', true); ?></h2>
		    		<p><?php echo get_post_meta($post->ID,'ranklab_footer_rotator_sub', true); ?></p>		
		    	<?php } else {  ?>
		        	<h2><?php echo stripslashes(get_option('ranklab_rotating_large')); ?></h2>
					<p><?php echo stripslashes(get_option('ranklab_rotating_sub')); ?></p>
				<?php } ?>
		    </div>
		    <div class="phone-c">
		        <h2 class="phone-text"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></h2>
		    </div>
		</div>
	</div><!-- end bottom-bar-left -->
	<div id="bottom-bar-right"> 
		 <a href="/contact/"><button class="btn-small btn-brown">Contact Us</button></a>
	</div><!-- end bottom-bar-right -->
</div><!-- end bottom-bar -->
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cycle2.scrollVert.min.js"></script>
<?php /*<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/lightbox.js"></script> */ ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.stellar.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/scripts.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.cookie.js"></script><!-- added this from old MH site for cookie -->


  <!-- this is the mobile popover from the old site -->
   <?php global $frn_mobile; ?>
   
    <?php if($frn_mobile == "Smartphone") {?>
   <div id="mobile_overlay" class="">
    <div id="mobileCTA">
		<div id="close_btn"><img src="<?php echo get_template_directory_uri(); ?>/style/images/close-icon.svg" /></div>
        <h3 class="text-center">Speak to a Professional</h3>
        <div class="text-center orange phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?></div>        
        <p>We care about your unique situation and needs. At Black Bear Lodge, our goal is to provide lifetime recovery solutions.</p>
    </div><!-- end mobile CTA -->
</div><!-- end mobile_overlay -->
    
    <script type="text/javascript">
   	 jQuery(document).ready(function() {
            if (jQuery.cookie('modal_shown') == null) {
                jQuery.cookie('modal_shown', 'yes', { expires: 7, path: '/' });
                setTimeout(function(){
                    jQuery('#mobile_overlay').delay(5000).addClass('visible');
					jQuery('#close_btn').click(function(){
					jQuery('#mobile_overlay').removeClass('visible');
						  });
            }, 4000);
        }
    });
    </script>
<?php } ?><!-- end frn function on for mobile detection on popover -->

</body>
</html>