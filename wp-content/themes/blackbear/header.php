<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title(''); ?></title>

<!-- dns prefetch -->
<link href="//www.google-analytics.com" rel="dns-prefetch">

<!-- meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">


<!-- icons -->
<link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/favicon.ico" rel="shortcut icon">
<link href="<?php echo get_template_directory_uri(); ?>/style/images/icons/touch.png" rel="apple-touch-icon-precomposed">
	
<!-- css + javascript -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/normalize.css" />
<!--
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/lightbox.css" /> -->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/jquery.sidr.dark.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/style/css/formatting.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=1.4" />
<script src="<?php echo get_template_directory_uri(); ?>/library/js/modernizr.min.js"></script>

<?php wp_head(); ?>
<script>
	//disables scroll zoom on google map 
	jQuery("body").ready(function($) {
		$('.map-container')
			.click(function(){
				$(this).find('iframe').addClass('clicked')})
		.mouseleave(function(){
				$(this).find('iframe').removeClass('clicked')});
	});
</script>

</head>
<body <?php body_class(); ?>>
<!-- header -->
<div class="header-container">
<header class="clearfix">
	<div id="mobile-header">
		<a id="responsive-menu-button" href="#sidr-main" onClick="ga('send', 'event', 'Hamburger Menus', 'Menu Opens & Closes'); ">Menu</a>
	</div>
	<div id="top-right-nav">
		<nav id="top-nav">
			<ul>
				<li class="top-nav-item"><a href="/resources/">Resources</a></li>
				<li class="top-nav-item"><a href="/inspiration/">Inspiration</a></li>
				<li class="top-nav-item"><a href="/blog/">Blog</a></li>
			</ul>
		</nav>
	</div><!-- end top-nav -->
	<div class="phone-bg"><span class="phone-icon-sm"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header"]'); ?></span></div>
    <!-- logo -->
	<div class="logo">
		<a href="<?php echo home_url(); ?>" title="">
			<img src="<?php echo get_template_directory_uri(); ?>/style/images/BBL-logo-dark.png" alt="Logo" title="" class="logo">
		</a>
	</div>
	<!-- /logo -->
	
	<div id="nav">
		<nav id="main-nav" role="navigation"><?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?></nav>
	</div><!-- end nav -->
</header>
</div><!-- end header-container -->