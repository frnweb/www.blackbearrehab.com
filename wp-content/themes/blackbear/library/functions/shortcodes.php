<?php

// Shortcodes

	// ROW
		function frn_foundation_row ( $atts, $content = null ) {
			return '<div class="row">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('row', 'frn_foundation_row' );
	///ROW

	// COLUMN
		function frn_foundation_col ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'size'		=> '',
				), $atts );
			return '<div class="' . esc_attr($specs['size']) . ' columns">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('col', 'frn_foundation_col' );
	///COLUMN

	// BLOCKGRID LIST ITEM
		function frn_foundation_blockgridli ( $atts, $content = null ) {
			return '<div class="column column-block">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('item', 'frn_foundation_blockgridli' );
	///BLOCKGRID LIST ITEM

	// BUTTON
		function frn_foundation_buttons ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'url'	=> '#',
				'style'	=> '', 
				'target'	=> '_self'
				), $atts );
			if(in_array('read-next', $specs, true)){
				return '<div class="read-next"><h4>Read This Next:</h4><a href="' . esc_attr($specs['url'] ) . '" class="button secondary ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
			} else {
			return '<a href="' . esc_attr($specs['url'] ) . '" class="button ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
			};
		}

		add_shortcode ('button', 'frn_foundation_buttons' );
	///BUTTON

	// FLEXVIDEO
		function frn_foundation_flexvideo ( $atts, $content = null ) {
			return '<div class="flex-video">' . $content . '</div>';
		}
		add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );
	///FLEXVIDEO

	// TABS
		function frn_foundation_tabs( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			if(in_array('alternate', $specs, true)){
				return '<div class="content-tabs alternate' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
			} else {
			return '<div class="content-tabs primary">' . do_shortcode ( $content ) . '</div>';
		};
		}

		add_shortcode ('tabs', 'frn_foundation_tabs' );
	///TABS

	// TAB MENU
		function frn_foundation_tabs_menu( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			static $i = 0;
			$i++;
			if(in_array('alternate', $specs, true)){
				return '<div class="menu-wrapper"><ul class="tab-menu alternate' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</ul></div>';
			} else {
			return '<div class="menu-wrapper"><ul class="tab-menu primary">' . do_shortcode ( $content ) . '</ul></div>';
		};
		}

		add_shortcode ('tab-menu', 'frn_foundation_tabs_menu' );
	///TAB MENU

	// TAB TITLE
		function frn_foundation_tabs_title ( $atts, $content = null ) {

		    $specs = shortcode_atts( array(
		        'class'     => ''
		    ), $atts );

		    static $i = 0;
		    if ( $i == 0 ) {
		        $class = 'class="current"';
		    } else {
		        $class = NULL;
		    }
		    $i++;
		    
		    return '<li class="tab' . $i . '"><a href="#"' . $class . ' '.esc_attr($specs['class'] ). '">' .
		    do_shortcode ( $content ) . '</a></li>';
		}

		add_shortcode ('tab-title', 'frn_foundation_tabs_title' );
	///TAB TITLE

	// TAB CONTENT
		function frn_foundation_tabs_content( $atts, $content = null ) {
			static $i = 0;
			$i++;
			return '<div class="tab-content-wrapper"' . $i . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('tab-content', 'frn_foundation_tabs_content' );
	///TAB CONTENT

	// TAB PANEL
		function frn_foundation_tabs_panel ($atts, $content = null ) {
			static $i = 0;
			if ( $i == 0 ) {
				$style = NULL;
			} else {
				$style = 'style="display:none;"';
			}
			$i++;
			return '<div class="dynamic tab' . $i . '" ' . $style . '>' . do_shortcode( $content ) . '</div>';
		}
		add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );
	///TAB PANEL

	// ACCORDION
		function frn_foundation_accordion( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
			), $atts );
			return '<div class="accordion">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('accordion', 'frn_foundation_accordion' );
	///ACCORDION

	// ACCORDION ITEM
		function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'title'		=> ''
				), $atts );
			static $i = 0;
		/*	if ( $i == 0 ) {
				$class = 'active';
			} else {
				$class = NULL;
			}*/
			$i++;
			$value = '<h3>' . esc_attr($specs['title'] ) . '<span class="plus">+</span></h3><div>' . do_shortcode( $content ) . '</div>';

			return $value;
		}

		add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );
	///ACCORDION ITEM


///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK

	// DIVIDER
		function line_divider() {

			return '<div class="divider clearfix"></div>';

		}
		add_shortcode( 'divider', 'line_divider' );
	///DIVIDER

	// CALLOUT
		function callout_box ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> ''
				), $atts );
			return '<div class="callout ' . esc_attr($specs['style'] ) . '">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('callout', 'callout_box' );
	///CALLOUT

	// CALLOUT CARD
		function callout_card ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'style'		=> '',
				'img'		=> ''
				), $atts );
			return '<div class="callout-card ' . esc_attr($specs['style'] ) . '"><div class="card-media" style="background-image: url('. esc_attr($specs['img'] ) .'"></div><div class="card-content">' . do_shortcode ( $content ) . '</div></div>';
		}
		add_shortcode ('callout-card', 'callout_card' );
	///CALLOUT CARD

	// LARGE TEXT
		function large_text ( $atts, $content = null ) {
			return '<div class="callout-text">' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('large_text', 'large_text' );
	///LARGE TEXT


	// MAP
		function map_container ( $atts, $content = null ) {
			return '<div class="map-container">' . $content . '</div>';
		}
		add_shortcode ('map', 'map_container' );
	///MAP

	// CONTACT BOX
		function contact_box_section ( $atts, $content = null ) {
			return '<div class="callout equalizer contact-box" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
		}
		add_shortcode ('contact_box', 'contact_box_section' );
	///CONTACT BOX

	// CONTAINER
		function div_container ( $atts, $content = null ) {
		    $specs = shortcode_atts( array(
		        'class'     => '',
		        ), $atts );
		    return '<div class="container ' . esc_attr($specs['class']) . ' ">' .  do_shortcode ( $content ) . '</div>';
		}

		add_shortcode ('container', 'div_container' );
	///CONTAINER

	//EMAIL US
		function email_us ($atts, $content = null) {
			$specs = shortcode_atts( array(
				'text'		=> 'Subscribe to our email list!',
				'domain'	=> 'Black Bear Post'
				), $atts );
			return '<div class="module email"><div class="email__card"><h3>' . esc_attr($specs['text']) . '</h3><form class="form form--email" target="_blank" action="http://cloud.frn-mail.com/subscribe" method="GET"><input placeholder="Email Address" id="input" class="form__input" type="text" name="email"><input name="domain" type="hidden" value="' . esc_attr($specs['domain']) . '"><button type="submit" value="Sign Me Up" class="button secondary">Sign Me Up</button></form></div></div>';
		}
		add_shortcode ('email', 'email_us' );
	//EMAIL US


//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-menu';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'email';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'facility_photo_slider';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';

    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// remove and re-prioritize wpautop to prevent auto formatting inside shortcodes
// shortcode_unautop is a core function

remove_filter( 'the_content', 'wpautop');
add_filter ( 'the_content', 'wpautop', 99 );
add_filter ('the_content', 'shortcode_unautop', 100 );

?>
