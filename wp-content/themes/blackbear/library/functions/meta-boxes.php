<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'ranklab_';

global $meta_boxes;

$meta_boxes = array();


// Page Options
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'page-options',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => 'Page Options',

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'page', 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		// Alt Title
		array(
			'name' => 'Alt Title',
			'id'   => "{$prefix}alt_title",
			'type' => 'text',
		),
		// Page Message
		array(
			'name' => 'Page Summary',
			'id'   => "{$prefix}page_summary",
			'type' => 'textarea',
		),
		// Footer Rotator
		array(
			'name' => 'Footer Rotating Large Text',
			'id'   => "{$prefix}footer_rotator_large",
			'type' => 'text',
		),
		// Footer Rotator Sub
		array(
			'name' => 'Footer Rotating Large Text',
			'id'   => "{$prefix}footer_rotator_sub",
			'type' => 'text',
		),
	),
);


/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function launch_register_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Meta_Box' ) )
		return;

	global $meta_boxes;
	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'launch_register_meta_boxes' );