<?php
// -------------------------------  Taxonomies ---------------------------------------

//Sidebar
register_taxonomy(  
	'sidebar',  
	array('page'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Sidebar',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
); 

?>