<?php
function post_type_gallery() {
    $labels = array(
        'name' => __( 'Gallery' ),
        'singular_name' => __( 'Gallery' ),
        'add_new' => __( 'Add New Gallery' ),
        'add_new_item' => __( 'Add New Gallery' ),
        'edit_item' => __( 'Edit Gallery' ),
        'new_item' => __( 'Add New Gallery' ),
        'view_item' => __( 'View Gallery' ),
        'search_items' => __( 'Search Gallery' ),
        'not_found' => __( 'No Gallery found' ),
        'not_found_in_trash' => __( 'No Gallery found in trash' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'capability_type' => 'post',
        //'rewrite' => array("slug" => "gallery"), // Permalinks format
        'menu_position' => 5,
        //'menu_icon' => plugin_dir_url( __FILE__ ) . '/images/calendar-icon.gif',  // Icon Path
        'has_archive' => false
    );
    register_post_type( 'gallery', $args );
}
add_action( 'init', 'post_type_gallery' );
?>