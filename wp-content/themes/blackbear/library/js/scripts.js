jQuery(document).ready(function($) {
	//Alternating Rows
	$("#subpage .sub_item:even").addClass("alt");
	
	/*
	//Responsive Nav
	$("#nav").addClass("js").before('<div id="menu">&#9776;</div>');
	$("#menu").click(function(){
		$("#nav").toggle();
		});
		$(window).resize(function(){
		if(window.innerWidth > 768) {
		$("#nav").removeAttr("style");
		}
	});
	*/
	
	//Content Formatting
	//Accordion
	$('div.accordion> div').hide();  
	  $('div.accordion> h3').click(function() {
	  	$(this).toggleClass('active');
	    $(this).next('div').slideToggle('fast');
	});
	
	//Tabs
	$('.tab-menu.primary li a').click(function(event) {
	    event.preventDefault();
	    $('.content-tabs.primary .dynamic').hide();
	    $('.tab-menu.primary li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});

	$('.tab-menu.alternate li a').click(function(event) {
	    event.preventDefault();
	    $('.content-tabs.alternate .dynamic').hide();
	    $('.tab-menu.secondary li a').removeClass('current');
	    var parent = $(event.target).parent();
	    $('.dynamic.' + parent.attr('class')).fadeIn();
	    parent.find('a').addClass('current');
	});
	
	//Equal Columns
	var max_height = 0;
	$("div.third").each(function(){
	    if ($(this).height() > max_height) { max_height = $(this).height(); }
	});
	$("div.third").height(max_height);
	
	//Stellar
	//if (document.documentElement.clientWidth < 800) {
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 40
		});
	// }
	
	
	//Responsive Nav
	$('#responsive-menu-button').sidr({
	name: 'sidr-main',
	source: '#nav, #top-nav'
	});
	
	
	
	

	
	
	
	
});
