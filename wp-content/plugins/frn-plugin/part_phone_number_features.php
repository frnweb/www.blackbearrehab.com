<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/*  TO DO INFINITY:

	FINISH FRN_SMS (function started at bottom of this)

	Program PHP solution for URL var triggers for Infinity pools (started, not finished)
*/

/*  NOTES: General notes and version updates about phone processing
	Functions in this file handle all phone number design/CSS situations while maintaining proper linking for mobile devices and analytics labeling.

	Although this file is seperate, most of the other files depend or reference something from this file. 
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17

	v6.3 - latest changes:
		Removed nowrap when text is defined
		Removed wasted processing by:
			Moving up everything that doesn't require phone number processing, 
			Moving variable definitions into the stages that refers to them,
			Moving up things that require minimal processing
			Relying on "return" to stop processing when what's remaining isn't necessary (instead of if/else)

	v7.1 - 9/14/17 - Added "label" to shortcode.

	v7.2 - 8/14/18 - Activated phone number linking for all devices by setting the default to TRUE. There is still one override left where phone numbers are specifically turned off.

	v7.3 - 10/18/18 - Added frn_sms shortcode since we are expanding our uses to having one per facilty. Too much for people to keep track of and it can change at any time.

	V8.0 - 12/11/18:
		Major overhaul -- completely different, similar logic
		General concepts:
			shorter, clarified functions
			modifying central attributes array instead of replicating intelligence in each function to determine device response
			integration of infinity pools into the main number function and linking function
*/
/*  USE CASES (for both shortcode and auto find):
	TEXT/CSS:
		MOBILE:
			Number digits
			Linked text options

		DESKTOP:
			Number digits
			Linked text options

	IMAGES:
		MOBILE:
			Link with number

		DESKTOP:
			Link with desktop setting (or none)
*/


include('part_phone_infinity.php'); //infinity functions


//////////
/// FEATURE CONTROLS
	function frn_phone_old_to_new($phones=""){ //old wp var to new
		//Many functions will rely on this comparison on first install/launch. 
		//Keeping it centralized to aid simplicity and when we deal with the same issue in the future
		$old_phone = get_option('site_phone_number');
		$phones = get_option('phones');
			if(!isset($phones)) $phones=""; if(!isset($old_phone)) $old_phone="";
			if($phones=="" && $old_phone!=="") $phones=$old_phone; //transition old to new if new is undefined
		return $phones;
	}
	function frn_phone_link_activate($p="") {
		/*
		$p=primary phone var array
		function allows ability to remove link for all numbers
		PHONE LINKING OPTIONS:
			PHP = PHP processing
			JS = JS processing (changes all numbers on page regardless of placement, not recommended and doesn't work as of 2017)
			N = Numbers only
			R = Remove numbers
		*/
		if($p=="") $p=frn_phone_old_to_new();
		if(!isset($p['phone_linking'])) $p['phone_linking']="";
		if($p['phone_linking']=="N") { //REMOVED: && ($p['auto_scan']=="D" || $p['auto_scan']=="") -- can't remember why this is here 10/26/18
			 return "no";
		}
		return "yes"; //yes is default
	}
/// END FEATURE CONTROLS
//////////


//////////
// PHONE NUMBER PROCESSING
	function frn_phone_number($number="",$sc="",$phones="") { //get phone number
		//Get site's main number (either infinity pool number or manually entered)
	   /* If the infinity system activated and if a pool savied into the DB, it overrides any manual number entry (because Infinity can only be activated manually, so pool already saved)
		* If Infinity deactivated, then only manual option allowed. 
		* If that's blank, then system deactivated  */
		if($phones=="") $phones=frn_phone_old_to_new();
		//echo "main number: ".$frn_main_number;
		$number=trim($number);
		
		if($number=="") {
			//if this processes, it means the number was not provided as an attribute in the shortcode, so we can do normal processing
			if(function_exists('frn_infinity_pool_num')) {
				$number = frn_infinity_pool_num($sc,$phones); //get number based on sc used
			}
			//if pool not set, then use manual number
			if($number=="") {
				//site_phone works for old and new approach
				if(!isset($phones['site_phone'])) $phones['site_phone']="";
				$number = $phones['site_phone']; 
			}
		}
		elseif(function_exists('frn_infinity_discovery')) {
			//if number attribute is not blank, then see if it's in list of infinity pool discovery numbers. If it is, make sure the format is one that Infinity can see.
			$number = frn_infinity_discovery($number);
		}

		return $number;
	}
	function frn_phone_digits_only($number="") {
		$number = trim(strip_tags(stripslashes($number))); //cleans out all HTML and quotes 
		//Discontinued 10/26/18 to allow for letters to be used in display numbers
		//$number = preg_replace('/\D/',"",$number); //cleans all but numbers (used second so that attributes with numbers are also removed)
		return str_replace(array(" ","(",")","-",".","+"),"",$number);
	}
	function frn_phone_switch_letters($number="") {
		//replace letters with numbers. (allows us to use 1-855-555-HELP)
		$digits=strlen($number);
		if($digits>=10&&$digits<=14) {
			$number = strtolower($number);
			$number = preg_replace("/a|b|c/","2",  $number);
			$number = preg_replace("/d|e|f/","3",  $number);
			$number = preg_replace("/g|h|i/","4",  $number);
			$number = preg_replace("/j|k|l/","5",  $number);
			$number = preg_replace("/m|n|o/","6",  $number);
			$number = preg_replace("/p|q|r|s/","7",$number);
			$number = preg_replace("/t|u|v/","8",  $number);
			$number = preg_replace("/w|x|y|z/","9",$number);
			$number = preg_replace("/\+/","0",     $number);
		}
		return $number;
	}
	function frn_phone_for_links($number="") {
		//Since 2015, we have not tested all device interpretations of phone number patterns to make sure a device recognizes the number and dials it.
		//At that time, the only format that was consistent among all smartphones and tablets was 1-###-###-####. 
		//This function merely makes the formatting consistent and replaces letters with numbers to make it dialable.

		$number = frn_phone_digits_only($number);

		//UNIQUE (i.e. what differentiates this from the frn_phone_format function)
		//if after stripping out everything but numbers and letters, it's blank then just pull the main number.
		if($number=="")  {
		   $number = frn_phone_number($number); 
		}
		$number = frn_phone_switch_letters($number); //many mobile devices don't translate letters in phone numbers (e.g. 1-855-345-HEAL)
		//END UNIQUE

		//NO "1" AT START of numbers - Infinity will not find the number in the link
		$number = frn_phone_format($number,"-");
		//DISCONTINUED: if(!is_numeric($number)) $number=preg_replace('/\D/',"",$number); //strips all but numbers in case some text is left -- discontinued 10/26/18
		
		return $number;
	}
	function frn_clean_number($number="") { //Old
		//Discontinued backup in case something was missed in random themes
		//old function name that didn't represent the purpose of the function
		//old name wasn't clear enough
		return frn_phone_for_links($number); 
	}
	function frn_phone_format($number="",$type="") {
		//Used to display the number in a particular format as needed
		//Most important for Infinity numbers since they are digits only
		if($number!=="") {
			$number = frn_phone_digits_only($number); //makes sure there is no HTML/PHP or formatting
			if($number!=="") { //it's possible there would be no number once code removed

				$digits=strlen($number);
				$format='$1-$2-$3'; //default

				if($type!=="") {
					if(    $type=="1()") $format='1 ($1) $2-$3';
					elseif($type=="()")  $format='($1) $2-$3';
					elseif($type=="1-")  $format='1-$1-$2-$3';
					elseif($type=="1.")  $format='1.$1.$2.$3';
					elseif($type==".")   $format='$1.$2.$3';
					elseif($type=="-")   $format='$1-$2-$3';
				}

				if($digits==11) {
					$pattern = '/^1?\d([a-zA-Z0-9]..)([a-zA-Z0-9]..)([a-zA-Z0-9]...)$/'; //starts with 1
					$number  = preg_replace( $pattern, $format, $number);}
				elseif($digits==10) {
					$pattern = '/([a-zA-Z0-9]..)([a-zA-Z0-9]..)([a-zA-Z0-9]...)$/'; //doesn't start with 1
					$number  = preg_replace( $pattern, $format, $number);}
				/* 
				//International removed 12/29/18 since there is no plan for it - reduced scenarios to test
				elseif($digits==13){
					$pattern = '/([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/'; //international
					$number = preg_replace($pattern,$format,$number);}
				elseif($digits==14 && substr($number,1)=="("){
					$pattern = '/([0-9]...)([0-9]..)([0-9]..)([0-9]...)$/'; //international
					$number = preg_replace($pattern,$format,$number);}
				*/
			}
		}
		return $number;
	}
	function frn_phone_url_tag_pairs() {
		//defines tag+value relationships to trigger features in other functions
		$t = array(
			array('tag' => 'st-t', 'value' => 'local'),
			array('tag' => 'ph', 'value' => 'bd'),
			array('tag' => 'ph', 'value' => 'local'),
			array('tag' => 'infinity', 'value' => 'local')
		);
		return $t;
	}
	function frn_phone_get_url_tag($tags=""){
		//not used yet. realized that 
		if(!is_array($tags)) $tags=frn_phone_url_tag_pairs();
		$i=0; $url_trigger="";
		while($url_trigger=="" && $i<count($tags)) {
			if(!isset($tags[$i]['tag'])) $tags[$i]['tag']="";
			$tag=$tags[$i]['tag'];
			if(!isset($_GET[$tag])) $_GET[$tag]="";
			$url_trigger = $_GET[$tag]; //blank if not found
			$tags[$i]['value'] = $_GET[$tag];
			//echo "<br />Dax: tag: ".$tags[$i]['tag']. "; value: ".$url_trigger;
			if($tags[$i]['value']!=="") {
				return $tags[$i];
			}
			$i++;
		}
		return "";
	}
// END Phone Number Processing
//////////




//////////
// FRN_PHONE PROCESSING/SHORTCODE:
	//Called by: 
		//infinity shortcode install function (frn_infinity_sc_install)
		//frn_footer
	function frn_phone($atts, $content="", $sc=""){
		//This becomes the main function for any situation to provide a phone number.
		//$atts = any attributes provided in the shortcode
		//$content = only when bookend approach used -- it holds whatever is between the bookends. Not used for this.
		//$sc = shortcode used to trigger this function (used for identifying the facility)
		
		/* NOTES:
			There are over 20 user/design scenarios this shortcode considers. 
				The following list attempts to define the options, but not the user scenario since there are too many to list here. 
				But comments throughout the code attempts to talk about the various scenario combinations as it tries to handle them.

			General overall:
				ID
				Class
				Inline Style
				Number (used to specify a number if different than FRN Settings)
				SPAN is used only when links are turned off
				Only = yes - returns the number as entered in the FRN Settings 
				Only = cleaned/link/mobile - formats in a link
				added are Analytics Event tracking values (predefined defaults, with overrides by use and device; e.g. if switching to a webpage link and text for desktops, Analytics event action switches to a version describing that situation)

			Text/CSS:
				title tag to clarify CSS icons if preferred to show when users hover
				Link text to phone number
				No Text/CSS Only
				Desktop Switch: Can use a link to a page instead of a phone number
				Nowrap style only if phone number displayed

			Images:
				ID
				CSS
				Inline style
				ALT tag
				Mobile users will get an image linked with the phone number
				Desktop user can get a link to a webpage or nothing at all
				(This doesn't apply for background images via CSS option. Use text option instead.)

			Removed:
				Only possible by device type (e.g. only mobile or desktop)
				This option removes all code by device type.

			Device Consideration:
				Mobile/Tablet
				Desktop/Laptops
				(Not OS specific)
				(Not browser specific)
		*/

		//Make sure no number is provided if phone_linking is set to have numbers removed
		$phones=frn_phone_old_to_new();
		if(!isset($phones['phone_linking'])) $phones['phone_linking']="";
		if($phones['phone_linking']=="R") {
			 return "";
		}



		/////
		// STAGE 1: SET DEFAULTS
			$wrapped=""; $wrap="###";
			/*
			$atts = shortcode_atts( array(
				//all possible attributes used in the shortcode should be listed here unless they should be ignored
				'number' => '',       //overrides default system numbers
				'only' => '',         //if yes, shows the number as enterd ("link" provides format for links)
				'text' => '',         //text linked instead of the number displayed (or options to have no text or cancel function) //common on many sites
				'url' => '',          //initial attribute for image_url, but since all other image attributes required "image", added image_url to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
				'image_url' => '',    //may be used on DD.org and old FRN.com (12/2018)
				
				/* All Default Vars
					
					'text' => '', //substitutes for number when linking
					
					//Defaults
					'id' => '', //applied to <A> tag for text/phone number versions, or <IMG> tags for images
					'class' => '', //applied to SPAN, not links
					'style' => '', //newer version since easier to remember -- used for inline styles
					'css_style' => '', //older version prior to 2016 -- used for inline styles
					'title' => '', //added to links only -- i.e. smartphones only

					//Mobile overrides
					'mobile' => '',
					'mobile_text' => '', //here in case someone mistakes this to be like the rest
					'mobile_id' => '',
					'mobile_class' => '',
					'mobile_category' => '', //overrides Analytics defaults
					'mobile_action' => '', //overrides Analytics defaults

					//Desktop overrides
					'desktop' => '',
					'desktop_text' => '', //here in case someone mistakes this to be like the rest
					'desktop_url' => '', //if desktop text defined, it can link the text with a URL to go to a contact us page, for example common on at least MH.com and Talbott
					'desktop_id' => '',
					'desktop_class' => '',
					'desktop_category' => '', //overrides Analytics defaults
					'desktop_action' => '', //overrides Analytics defaults

					//Analytics defaults
					'category' => '', //updated 2016 since other versions hard to remember, "ga_phone_category" version assigned to this later in code
					'action' => '', //updated 2016 since other versions hard to remember, "ga_phone_location" version assigned to this later in code
					'label' => '', //added 9/14/17 due to Rehab-International.org facility numbers
					'ga_phone_category' => 'Phone Numbers',
					'ga_phone_location' => 'Phone Clicks [General]',
					'ga_phone_label' => 'Calls', //changed to phone number if different than site default

					
					'image_id' => '', //ID will be used if this is blank
					'image_class' => '',
					'image_style' => '',
					'image_title' => '',
					'alt' =>'', //was initial use but since all other image attributes required "image", added image_alt to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
					'image_alt' =>'',


					//Likely not used anymore (prior to 2015)
					'intl_prefix'=> '', //was used on PPC only when testing international numbers -- made regex too hard when detecting number in text blocks
					'frn_number_style' => '' //old option some rare situations may still use -- now just assigned to inline $style if it isn't defined
				*/
			//), $atts );

			$defaults = array(
				'number' => '',       //overrides default system numbers
				'only' => '',         //if yes, shows the number as enterd ("link" provides format for links)
				'text' => '',         //text linked instead of the number displayed (or options to have no text or cancel function) //common on many sites
				'url' => '',          //initial attribute for image_url, but since all other image attributes required "image", added image_url to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
				'image_url' => ''    //may be used on DD.org and old FRN.com (12/2018)
			);
			//although defaults blank, this process also creates vars used in later functions
			$atts=wp_parse_args($atts,$defaults); 
			extract( $atts ); //turn all array keys into vars for the rest of the function
		// END STAGE 1
		/////


		/////
		// STAGE 2: DESKTOP TEXT REMOVE OPTION
			//Initially the text=remove option only applied to desktops, but it eventually became hard to remember the unique case so we switched to desktop_text.
			//Unfortunately cases could still be out there, so this handles those. We'd never want text=remove to apply to all devices anyway.
			$test_text=""; global $frn_mobile;
			if($text!=="") $test_text=strtolower($text);
			if($test_text=="remove" && !$frn_mobile) {
				return "";
			}
			elseif($test_text=="remove") {
				//since only mobile devices get this far, we need to remove "remove" from the text vars
				//Initially, text was only supposed to be used for desktops
				//This accounts for early desktop-only planned uses of "text=remove"
				$text="";
				$test_text="";
			}
		// END STAGE 2
		/////
		

		/////
		// STAGE 3: SIMPLIFY ATTRIBUTES: 
			//text, id, and class attributes have changed over the years and some are device specific
			//These function simplify attributes into the basic text, id, and class attributes used for triggering farther down the process.
			//These return an updated $atts array 
			//FOR TESTING:
				//echo "<br /><br /><b>".$sc."</b><br /> Initial Attributes: <br />"; 
				//print_r($atts);
			//test_text lets us change it to lowercase without changing display
			
			$atts = frn_phone_text($atts);
				//echo "<br /><br />After Text: <br />"; 
				//print_r($atts);
			$atts = frn_phone_ids($atts); 
			$atts = frn_phone_classes($atts);
			//print_r($atts);
			extract( $atts ); //turn all array keys into vars for the rest of the function
		// END STAGE 3
		/////


		/////
		// STAGE 4: CANCEL DISPLAY: Mobile device cancel/remove. 
			//Stage 2 handled this for desktops. 
			//If text=remove at this point, then it means it needs to be removed for mobile
			//the text function above moves device specific text attributes to the main text attributes
			//that's why we can reprocess the same variable.
			//test_text needs to be redefined now that text is also
			//keeps the original from being changed for later processing
			if($text!=="") $test_text=strtolower($text); 
			if($test_text=="remove") {
				return "";
			}
		// END STAGE 4
		/////


		/*////
		// STAGE 5: GET NUMBER (Considers infinity pool)
			//this is the point where the shortcode used to trigger this function is what determines the Infinity pool selected
			//number attribute overrides this option
			//Infinity pool trumps manually provided number
			//if Infinity not activated, no matter the shortcode used, it defaults to the manual number//
			//If Infinity activated but pools are not, this entire system will function normally (i.e. the Talkdesk number will be placed into the HTML), but Infinity's JS will not replace the number with a pool
			//returns number as entered only; display not considered at this stage
			*/
			if($number=="") {
				$number = frn_phone_number($number,$sc,$phones); 
			}
			elseif(function_exists('frn_infinity_number_replace')) {
				//if number attribute is not blank, then see if it's in list of old numbers and replace with facility pool discovery number
				$number = frn_infinity_number_replace($number,$phones);
			}
		// END STAGE 5
		/////


		/////
		// STAGE 6: UPDATE NUMBER VAR in $ATTS
			//Due to $atts provided, this var is blank unless already provided via SC attribute.
			//Otherwise, the "number" var in atts will always be blank. Resetting here.
			//Even if already provided, $set_number and $number will already be the same
			if($number!=="") {
				$number_array = array(
					'number' => $number,
				);
				$atts = wp_parse_args($number_array,$atts); 
			}
		// END STAGE 6
		/////


		/////
		// STAGE 7: DISPLAY OF ITEM
			/*	CASES:
				number only formats
				Text only:
					if empty (leaves wrap only)
					text present (wraps with wrapper)
					both devices (default when "text" used)
					desktop-special displays (eg. desktop_text)
					mobile-special displays (eg. mobile_text)
				image displays
				INCLUDES: 
					Event clicks tagging (general or device/use specific)
					Classes/IDs
					Link specific features
			*/

			//STAGE 7-A: NO LINK, ONLY NUMBER
			//If "only" is used -- the simplest processing
			$only=trim($only);
			//$only="cleaned"; 
			if($only!=="") {
				//returns number according to value provided
				return frn_phone_only($number,$only); 
			}


			//STAGE 7-B: TEXT DISPLAY
			//If no image, use text if it's filled out
			if($test_text=="empty") {
				//translates $text returned above
				$wrapped = "";
			}


			//STAGE 7-C: IMAGE DISPLAY: Determine if image is used
			//DD.org and maybe on old FRN.com
			//Check if image URL present
			if($text=="" && $test_text!=="empty") {
				$url=trim($url);
				if($url!=="") $image_url=$url;
				$image_url=trim($image_url);
				if($image_url!=="") {
					$wrapped = frn_phone_image($atts);
				}
			}
			elseif($test_text!=="empty") {
				$wrapped = $text;
			}


			//STAGE 7-D: NUMBER DISPLAY: Use number if no text
			if($wrapped=="" && $test_text!=="empty") {
				//translates $number provided above -- returns number as entered
				$wrapped=$number;
			}

		// END STAGE 7
		/////


		/////
		// STAGE 8: LINK AND SPAN
			$wrap = frn_phone_link($atts,$sc);
			$wrap = frn_phone_span($atts,$wrap,$wrapped); //only used when linking disabled; may never use (previously was desktop only)
		// END STAGE 8
		/////


		/////
		// STAGE 9: RETURN CONTENT
			return trim(str_replace("###",$wrapped,$wrap)); //replace "###" with whatever should display ($wrapped)
		// END STAGE 9
		/////

	}

	//Due to "frn_phone" now in the Infinity pools array, it takes care of registering the shortcode
	//this is a backup if that's disabled for some reason
	if ( !shortcode_exists( 'frn_phone' ) && !is_admin() ) {
	    add_shortcode( 'frn_phone', 'frn_phone' );
	}
// END SHORTCODE
//////////


//////////
// PHONE_DISPLAY Processing
	function frn_phone_text($atts){
		//Due to all the attribute options available, this function helps do the basic thinking and simplifies this core process.
		//all remaining functions will just work off the core "text" var. This functions makes sure that's set as appropriate for each device.

		//Previously defined arrays (even if they were merely defined by defaults at this point) will overwrite these
		$defaults = array(
			'text' 			=> '', //if the person wants text to be linked instead of the number displayed
			'mobile' 		=> '',
			'mobile_text' 	=> '', //here in case someone mistakes this to be like the rest
			'desktop' 		=> '',
			'desktop_text' 	=> '' //here in case someone mistakes this to be like the rest
		);
		//combine arrays, but ignore defaults if previously defined
		$atts=wp_parse_args($atts,$defaults); 
		//echo "<br />Inside Text Function: <br />";
		//print_r($atts);
		extract($atts); //turn keys into vars
		//return $text;
		
		//Process text by device
		global $frn_mobile;
		$text = trim($text);
		if(strtolower($text)=="remove" && $frn_mobile) $text=""; //Keeps the number from being removed on mobile devices. text=remove was initiatially meant only for desktops
		if(!$frn_mobile && $text=="") {
			//consolidate text values for desktop scenarios only (used to determine linking URL)
			$desktop = trim($desktop);
			if($desktop=="") {
				$desktop = trim($desktop_text);
			}
			$text = $desktop;
		}
		elseif($frn_mobile && ($mobile!=="" || $mobile_text!=="")) {
			$mobile = trim($mobile);
			if($mobile=="") {
				$mobile = trim($mobile_text);
			}
			$text = $mobile;
		}

		//because $text_text is already defined, we can clear this out when empty is present.
		if($text=="empty") $text="";


		/*
		//Old logic
		if (  (!$frn_mobile && $test_text=="remove" )  ||  ($frn_mobile && $test_text_m=="remove") || (!$frn_mobile && $test_text=="empty" && $id=="" && $class=="")  ) {
			//TESTING: return "<h1>Stage 1: Desktop only. Text or Desktop set to 'remove'. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";
			return "";
		}
		*/
		
		//update array
		//I'd rather remove the old atts, but being there doesn't really add much processing power. 
		$text_array=array(
			'text' => $text,
			//clean up array
			'mobile' => $mobile,
			'mobile_text' => '',
			'desktop' => $desktop,
			'desktop_text' => ''
		);
		return array_merge($atts,$text_array); //atts must be first so that our new empty "text" values overwrite the default
	}
	function frn_phone_only($number="",$only=""){
		$only=strtolower($only);
		if    ( $only=="yes" ) {
			return $number; //returns number as entered
		}
		elseif( $only=="cleaned" || $only=="mobile" || $only=="link" || $only=="links" ) {
			return frn_phone_for_links($number); //various settings that mean the same
		}
		elseif( $only=="digits" ) {
			return frn_phone_digits_only($number);
		}
	}
	function frn_phone_link($atts="",$sc="") {

		$link_activated = frn_phone_link_activate();

		if($link_activated!=="yes") return "###";
		else {
			//only set defaults for vars NOT previously defined
			//due to array_merge function, array keys defined here will ALWAYS overwrite what's in the atts array
			$defaults = array(
				'style' => '', //newer version since easier to remember -- used for inline styles
				'css_style' => '', //older version prior to 2016 -- used for inline styles (used only niche sites)
				'title' => '', //added to links
				'text' => '', //only used in this function to determine how to use the "URL" var
				'url' => '', //only used in this function when combined with the "text" var for desktop only. 
				'image_url' => '', //used here to identify non-number wrapped situation

				//Device overrides
				'desktop_url' => '', //when "text" provided, this switches link around number to be this instead of the typical tel: link
				'desktop_category' => '',
				'desktop_action' => '',

				//Likely not used anymore
				'frn_number_style' => '',
				'intl_prefix' => '',
			);
			//combine arrays, but ignore defaults if previously defined
			$atts=wp_parse_args($atts,$defaults); 
			extract($atts); //turn keys into vars

			$url  = frn_phone_url($number,$text,$atts); //combine url attributes and device intelligence
			
			//CONSOLIDATE DEVICE SPECIFIC TEXT OPTIONS (to determine URL var use)
			$number=frn_phone_for_links($number);

			//inline styles
			if($style=="" && $css_style!=="") $style=$css_style;
			if($frn_number_style!=="" && $style=="") $style=$frn_number_style; //old approach no one could remember
			if(strtolower($style)=="none") $style="";


			//Manually provided URLs are only used in this linking function when a text attribute is provided (otherwise it's assumed it's meant to be an image's src url)
			//intention was for the URL var to only be needed for desktop scenarios since mobile devices need a phone number to be linked, while we may want to switch to a contact link for desktops
			global $frn_mobile;
			if(!$frn_mobile && $text!=="") {
				$url=trim($url);
				$desktop_url=trim($desktop_url); //used to link the text to a page on the site
				if($desktop_url!=="" && $url=="") $url=$desktop_url;
			}


			///////
			//likely no longer used -- prior to 2015
			$intl_prefix=trim($intl_prefix);
			$frn_number_style=trim($frn_number_style);


			$title=trim($title);

			

			//////
			// Infinity Main Values
			global $inf_main; $inf_class=""; $inf_data="";
			if(!isset($inf_main['activate'])) {
				$inf_main['activate']=""; 
				$inf_main['class']=""; 
				$inf_main['data']="";
				$inf_main['class_text']=""; 
				$inf_main['data_text']="";
			}
			$space=""; $sms=(stripos($sc, "_sms")) ? "yes" : "no";
			if(!$frn_mobile && $desktop_url!=="" && $image_url=="") {
				//Matches intelligence below to avoid confusion
				//Infinity classes and data should not be activated in this case
			} 
			elseif($sms=="no") {
				if($inf_main['activate']!=="") {
					//no isset needed. Values set whether activated or not. Blank if not activated.
					$inf_class = $inf_main['class'];
					$inf_data  = $inf_main['data'];
					if($text!=="" || $image_url!=="") {
						$inf_class = $inf_main['class_text'];
						$inf_data  = str_replace("###",$number,$inf_main['data_text']);
					}
					if($class!=="" && $inf_class!=="") $space=" ";
					$class=$class.$space.$inf_class;
				}
			}



			////////
			// CLASS AND ID
			if($id!=="") $id=' id="'.$id.'"';

			$space=""; $default_class = "frn_phone";
			if($class!=="") $space=" ";
			if($sms=="yes") $default_class = "frn_sms";
			$class=' class="'.$default_class.$space.$class.'"';


			//Load Settings
			$phones=frn_phone_old_to_new();

			///////
			// Desktop Link Custom
			if(!$frn_mobile && $desktop_url!=="" && $image_url=="") : 
				//TESTING: return "<h1>Stage 2: Desktop only. Text version. URL switcheroo. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";
				//When this shortcode is used on a page like a normal link, it's no longer a conversion element
				//so it should be reported as a normal click to another page and NOT as a Google click event.
				//However, this can be overridden if we use the word "desktop" or "global" somewhere in the category label. (Global assumes it's used in our global contact options)
				if(!isset($desktop_action)) $desktop_action="";
				if($desktop_action=="") $desktop_action = "Desktop Switch with: ".$desktop_url;
				$desktop_ga_defaults = array(
					'action' => $desktop_action,
					'label' => "Contact"
				);
				$atts = array_merge($atts,$desktop_ga_defaults);
				$ga=frn_ga_click_event_js($number,$atts);
				
				//Since this is a link, it'll work like a normal pageview, but I wanted to record the event so we can evaluate contact options performance in one place
				return '<a href="'.$desktop_url.'" '.$id.$class.$ga.' >###</a>'; 
				//onClick="if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \'Desktop Phone Number Switch with: '.$desktop_url.'\');" >'.$desktop.'</a>';

			endif;


			////////
			// All Purpose Link for Wrapping Numbers
			if($phones['phone_linking']!=="JS") :
				//JS version handles all of the GA code and wrapping numbers (if it actully worked still - it's likely a quick fix)
				
				//default nowrap removed since we now have a more advanced team
				//if($style=="" && $text!=="") $style = ' style="white-space:nowrap;"'; //default since it's often forgotten
				//elseif($text=="" && $style=="") $style=""; //more clears out options
				if($style!=="") $style = ' style="'.$style.'"';
				elseif($text!=="") $style=""; //avoids nowrap number default
				else $style = ' style="white-space:nowrap;"';

				if($title!=="") $title = ' title="'.$title.'"';

				$pool_name=""; 
				if(function_exists('frn_infinity_pool_name_by_number')) {
					$pool_name = frn_infinity_pool_name_by_number($number,$phones); 
				}
				if($pool_name!=="") $number_label = $pool_name;
				else $number_label = $number;
				$ga=frn_ga_click_event_js($number_label,$atts);
				//do not include "return false in an onClick unless you don't want the href followed"
				$pre=($sms=="yes") ? "sms" : "tel" ;
				$start = ' <a '.$id.$class.$title.' href="'.$pre.':'.$number.'"'.$style.' '.$inf_data.$ga.' >'; //if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \''.$ga_phone_location.'\''.$ga_phone_label.'\'
				$end = "</a>";
			
			else : 
				//used for JS processing of numbers ONLY
				if($intl_prefix!=="") {
					$intl_prefix=' intl_prefix="'.$intl_prefix.'"';
				}

			endif;


			return $start . "###" . $end;

		}
	}
	function frn_phone_span($atts,$wrap="",$wrapped="") {
		if($wrap=="###") {

			$defaults = array(
				'style' => '',
				'css_style' => ''
			);
			//combine arrays, but ignore defaults if previously defined
			$atts=wp_parse_args($atts,$defaults); 
			//print_r($atts);
			extract($atts); //turn keys into vars
			
			//STYLE FEATURES
			$style=trim($style);
			$css_style=trim($css_style); //the old version likely used on niche sites prior 2016
			if($css_style!=="") $style=$css_style; 
			$space=""; //if nothing provided, no space between classes needed

			if(function_exists('frn_infinity_activate')) {
				global $inf_main;
				if(!isset($inf_main['activate'])) {
					$inf_main['activate']=""; 
					$inf_main['class']=""; 
					$inf_main['data']="";
					$inf_main['class_text']=""; 
					$inf_main['data_text']="";
				}
				//no isset needed. Values set whether activated or not. Blank if not activated.
				$inf_class = $inf_main['class'];
				$inf_data  = $inf_main['data'];
				if($text!=="") {
					$inf_class = $inf_main['class_text'];
					$inf_data  = $inf_main['data_text'];
				}
				if($class!=="" && $inf_class!=="") $space=" "; //space needed between them
				$class=$class.$space.$inf_class; //add in infinity class
			}
			
			if($class!=="") $space=" ";
			$class=' class="frn_phone'.$space.$class.'" ';
			if($id!=="") $id=' id="'.$id.'" ';
			$span_style="";
			if($style=="") {
				if($text=="" && frn_phone_digits_only($wrapped)==frn_phone_digits_only($number)) {
					$span_style=' style="white-space:nowrap;" ';
				}
			}
			else {
				$span_style=$style;
			}

			$start = '<span'.$id.$class.$span_style.$inf_data.'>';
			$end = "</span>";

			return $start . "###" . $end;
		}

		return $wrap;
	}
	
	function frn_phone_image($atts) {

		$defaults = array(
			//Image options for when phone numbers are used in them (very popular on niche sites)
			'url' => '', //initial attribute, but since all other image attributes required "image", added image_url to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
			'image_url' => '',
			'image_id' => '', //ID will be used if this is blank
			'image_class' => '',
			'image_style' => '',
			'title' =>'',
			'image_title' => '',
			'alt' =>'', //was initial use but since all other image attributes required "image", added image_alt to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
			'image_alt' =>'',
			'style' =>'',
		);
		$atts=wp_parse_args($atts,$defaults); 
		extract($atts); //turn keys into vars

		$url=trim($url);
		if($url!=="") $image_url=$url;
		$image_url=trim($image_url);

		if($image_url!=="") {
			
			//TESTING: return "<h1>Stage 3da: Mobile or Desktop // Image ONLY // No onlys // no 'remove' vars // desktop_url is empty (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text.")</h1>";

			//consolidate image vars into one var
			if($alt!=="") $image_alt=$alt;
			if($title!=="") $image_title=$title;

			//Setting CLASS and ID
			//we specifically set image_id/class to keep them out of phone number processing and leave the others blank
			//initially, we used image_id, but it became too complex for designers to remember, so sticking with just ID and class now (as of 2015)
			if($image_id=="" && $id!=="") {$image_id=$id; $id="";} //we can't use ID since it's passed to the link. For images, we don't want IDs in links
			if($image_id!=="") $image_id=' id="'.$image_id.'"';
			
			if($class!=="" && $image_class=="") {$image_class=$class; $class="";}
			if($image_class!=="") $image_class=' class="'.$image_class.'"';
			
			//inline style for number only
			if(strtolower($style)=="none") $style="";
				elseif($style!=="") $style=' style="'.$style.'"';
			
							/*
							/// NOT USED ANYWHERE BY 2016 at earliest -- old niche sites only
							//Check if domain bases are used (likely deprecated as of 2016, but didn't search 2015 redesigned sites for usage, so had kept activated)
							if(stripos($image_url,"%%frn_imagebase%%")>=0 or stripos($image_url,"%%idomain%%")>=0) {
								$options_sitebase = get_option('site_sitebase_code');
								$site_base = site_url();
								if(isset($options_sitebase['frn_imagebase'])) {
									if(trim($options_sitebase['frn_imagebase'])!=="") $site_base =  trim($options_sitebase['frn_imagebase']);
								}
								$image_url=str_replace('%%frn_imagebase%%',$site_base,$image_url);
								$image_url=str_replace('%%idomain%%',$site_base,$image_url);
							}
							else if(stripos($image_url,"%%frn_sitebase%%")>=0 or stripos($image_url,"%%ldomain%%")>=0) {
								$options_sitebase = get_option('site_sitebase_code');
								$site_base = site_url();
								if(isset($options_sitebase['frn_sitebase'])) {
									if(trim($options_sitebase['frn_sitebase'])!=="") $site_base =  trim($options_sitebase['frn_sitebase']);
								}
								$image_url=str_replace('%%frn_sitebase%%',$site_base,$image_url);
								$image_url=str_replace('%%ldomain%%',$site_base,$image_url);
							}
							*/

			if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") $image_url = str_replace('http://','https://',$image_url);}
			

			//Check if phone number FRN %% shortcode used in alt tag and title
			if($image_alt!=="") {
				$image_alt=str_replace('%%frn_phone%%',$number,$image_alt);
				$image_alt=' alt="'.$image_alt.'"';
			}
			if($image_title!=="") {
				$image_title=' title="'.$image_title.'"';
				$image_title=str_replace('%%frn_phone%%',$number,$image_title);
				$title=str_replace('%%frn_phone%%',$number,$title);
			}
			
			//build image HTML
			return '<img'.$image_id.$image_class.' src="'.$image_url.'"'.$image_title.$image_alt.$image_style.' />';
			
			/* From Old version
			//Prep it all by sending to the phone prep function to build it
			if($frn_mobile) 
				return frn_phone_link(array($image,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$id,$title,$lhn_tab_disabled,$intl_prefix));
			else {
				if($desktop_url!=="") return '<a href="'.$desktop_url.'">'.$image.'</a>';
				else return $image; 
			}
			*/

		}
	}
	function frn_phone_url($number="",$text="",$atts="") {
		/*
		URL Cases:
			URL + (text OR image_url) = desktop version only
			desktop_url + text = desktop version only
			image_url = triggers the system to know that an image will show -- for mobile, it's linked with phone number. For desktop, linked to url provided
		*/
		//although blank, this makes sure the keys for this function are turned into vars when empty
		$defaults = array(
			'url' => '',
			'image_url' => '',
			'desktop_url' => ''
		);
		//combine arrays, but ignore defaults if previously defined
		$atts=wp_parse_args($atts,$defaults); 
		extract($atts); //turn keys into vars

		global $frn_mobile;
		if(!$frn_mobile && $text!=="") {
			$url=trim($url);
			$desktop_url=trim($desktop_url); //used to link the text to a page on the site
			if($desktop_url!=="" && $url=="") $url=$desktop_url;
		}

		if(!$frn_mobile && $desktop_url!=="" && $image_url=="") {}

		return $url;
	}
	
	function frn_phone_ids($atts){
		$defaults = array(
			'id' => '', //applied to <A> tag for text/phone number versions, or <IMG> tags for images
			'mobile_id' => '',
			'desktop_id' => ''
		);
		//combine arrays, but ignore defaults if previously defined
		$atts=wp_parse_args($atts,$defaults); 
		extract($atts); //turn keys into vars

		global $frn_mobile;
		if($frn_mobile && $mobile_id!=="") {
			$mobile_id = trim($mobile_id);
			if($mobile_id!=="") {
				$id = $mobile_id;
			}
		}
		elseif(!$frn_mobile && $desktop_id!=="") {
			$desktop_id = trim($desktop_id);
			if($desktop_id!=="") {
				$id = $desktop_id;
			}
		}
		else {
			$id = trim($id);
		}

		//update array
		//I'd rather remove the old atts, but being there doesn't really add much processing power. 
		$new_array=array(
			'id' => $id,
			//clean up array
			'mobile_id' => '',
			'desktop_id' => ''
		);
		return array_merge($atts,$new_array); //atts must be first so that our new empty "text" values overwrite the default
	}
	function frn_phone_classes($atts){
		$defaults = array(
			'class' => '', //applied to SPAN, not links
			'mobile_class' => '',
			'desktop_class' => ''
		);
		//combine arrays, but ignore defaults if previously defined
		$atts=wp_parse_args($atts,$defaults); 
		extract($atts); //turn keys into vars

		global $frn_mobile;
		if($frn_mobile && $mobile_class!=="") {
			$mobile_class = trim($mobile_class);
			if($mobile_class!=="") {
				$class = $mobile_class;
			}
		}
		elseif(!$frn_mobile && $desktop_class!=="") {
			$desktop_class = trim($desktop_class);
			if($desktop_class!=="") {
				$class = $desktop_class;
			}
		}
		else {
			$class = trim($class);
		}

		//update array
		//I'd rather remove the old atts, but being there doesn't really add much processing power. 
		$new_array=array(
			'class' => $class, 
			//clean up array
			'mobile_class' => '',
			'desktop_class' => ''
		);
		return array_merge($atts,$new_array); //atts must be first so that our new empty "text" values overwrite the default
	}
// END PHONE DISPLAY PROCESSING
//////////




//////////
// CONTENT AUTOSCAN: 
	//If turned on, this will scan only content or the entire page for phone number patterns for only mobile devices and turn touches on them into trackable events and to always look like links.
	add_filter( 'the_content', 'frn_phone_autoscan_content');
	function frn_phone_autoscan_content( $content ) {
		/*
		To properly replace nmbers and not numbers in links, we need to consider outside characters when searching, 
		But only replace the numbers inside that.
		This function is stage 1 or 2

		U.S. Versions:
		0 (000) 000-0000
		+0-000-000-0000
		0-000-000-0000
		000-000-0000
		(000) 000-0000
		000-0000

		extensions not found
		Some international versions might be found, but most likely only part of the number

		
		NOTE: To help with the autoscan, it requires a period, space, bracket, greater or less than, or parenthesis to be at the front and/or end of the number before it's found. 
			  Some links have numbers with periods in them that cause the autoscan feature to add the SPAN code within the links. This decreases the chances of that happening, but it also includes those characters in the linked text, but not the telephone link.
			  Primary resources: http://us3.php.net/preg_replace and http://us3.php.net/manual/en/function.preg-replace-callback.php
			  A phone pattern resources: http://stackoverflow.com/questions/17331386/php-preg-replace-phone-numbers ,  http://www.myspotinternetmarketing.com/javascript-php-and-regular-expressions-for-international-and-us-phone-number-formats/
		
		Prior to January 31, 2019, this function was only activated for mobile devices.
		*/
		$phones = frn_phone_old_to_new();
		$auto_scan="C";
		if(isset($phones['auto_scan'])) $auto_scan = $phones['auto_scan'];
		if($auto_scan!=="W" && $auto_scan!=="DA") { // && $auto_scan!=="A" Removed since the JavaScript entire page scanning isn't working.
			//$us_pattern_in_content = '/^(?:(?:(?:1[.\/\s-]?)(?!\())?(?:\((?=\d{3}\)))?((?(?!(37|96))[2-9][0-8][0-9](?<!(11)))?[2-9])(?:\((?<=\(\d{3}))?)?[.\/\s-]?([0-9]{2}(?<!(11)))[.\/\s-]?([0-9]{4}(?<!(555(01([0-9][0-9])|1212))))(?:[\s]*(?:(?:x|ext|extn|ex)[.:]*|extension[:]?)?[\s]*(\d+))?$/';
			//$pattern_in_content = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/s"; //makes sure there is a space or some other character on either side of the number
			//$pattern_in_found = "!((\d )|(\d-))?(\(|\[)?(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i";
			//old (links the characters on either side of number as well: $content = preg_replace($pattern, $before."$0".$after, $content, 1);
			$dax_custom_number_wrap="/[\s >]?(1?[- .]?\(?\d\d\d\)?[- .]\d\d\d[- .]....)[<\s.!? ]/";
			$content = preg_replace_callback($dax_custom_number_wrap, 'frn_phone_autoscan_num_replace', $content);
		}
		//preg_match_all($dax_custom,$content,$phone_found);
		//print_r($phone_found);
		return $content;
	}
	function frn_phone_autoscan_num_replace($number_wrap) {
		//This second stage regex searches within the outer characters so that only the number is replaced and not outer characters
		//$pattern_in_found_intl = "!((\d )|(\d-))?(\(|\[)?(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i";
		$dax_custom_number_core="/(1?[- .]?)\(?\d\d\d\)?[- .]\d\d\d[- .]..../";
		return preg_replace_callback($dax_custom_number_core, 'frn_phone_autoscan_num_link', $number_wrap[0]);
	}
	function frn_phone_autoscan_num_link($matches) {
		//This third stage is the function that actually links the number
		//echo "space".$matches[1]."found";
		$atts = array(
			"number" => trim($matches[0])
		);
		//due to regular expression, if a number doesn't have a 1 in front within a sentence, 
		//then the space before it is included in the found set and then removed by a trim()
		//if a space is found, then we need to add it back in so the numbers aren't right against the prior word.
		$space_test=""; $space="";
		if(isset($matches[1])) $space_test=$matches[1];
		if($space_test==" ") $space=$space_test;
		return $space.frn_phone($atts); //OLD: frn_phone_link(array($matches[0]));
	}

	add_filter('widget_text', 'frn_phone_autoscan_widget');
	function frn_phone_autoscan_widget( $widget ) {
		//PHP option: simply acts like a shortcode in that it looks for a number pattern and then replaces it with the spans and number again
		//Prior to January 31, 2019, this function was only used for mobile devices.
		$phones = frn_phone_old_to_new();
		$auto_scan="C";
		if(isset($phones['auto_scan'])) $auto_scan = $phones['auto_scan'];
		if($auto_scan=="WC" || $auto_scan=="W") {
			//Pull the matches
			$pattern = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/";
			$widget = preg_replace_callback($pattern, 
				function($matches){
					$atts = array(
						"number" => $matches[0]
					);
					return frn_phone($atts); //OLD: frn_phone_link(array($matches[0]));
				}, $widget, 1);
		}
		return $widget;
	}
// END CONTENT AUTOSCAN
//////////




//////////
// OLD NUMBERS LIST ARRAY

	//List is ordered by the numbers most likely to be found by site or webpage traffic.
	//Once a number is found, the system stops looping.
	//If "pool" is in parenthesis, that's what helps match the right pool to the number.
	//You'll see a "kw" provided in the pool array. It's used to search these old codes and sometimes I have added words to make sure the right pool is selected.
	function frn_phone_old_numbers() {
		$old_numbers = array(
			array('8773453357','Web Dual [Pool:Network]'),
			array('8558086212','Web Black Bear'),
			array('8773458494','Web MHouse'),
			array('3105268770','Web Canyon'),
			array('8773453299','Web Canyon'),
			array('8777141318','Web FRN [Pool:Network]'),
			array('8558943703','Web Talbott'),
			array('8773451887','Web The Oaks'),
			array('8553178377','Web Skywood'),
			array('8777141322','Web Alumni [Pool:LC Accepted]'),
			array('8883124220','Web Heroes'),
			array('8774690675','Web MHouse (DT)'),
			array('8779121740','Web MHouse (DT)'),
			array('8777752162','Web MHouse (DT)'),
			array('8777651598','Web MHouse (DT)'),
			array('8779378491','Web MHouse (DT)'),
			array('8668094372','Web The Oaks (DT)'),
			array('3125964088','Web OP (Chicago)'),
			array('8553178259','Web OP (Chicago)'),
			array('2489656614','Web OP (Detroit)'),
			array('8553178257','Web OP (Detroit)'),
			array('8184641325','Web OP (Encino)'),
			array('8553178258','Web OP (LA) [Encino]'),
			array('8558086205','Web OP (Memphis)'),
			array('9015056519','Web OP (Memphis)'),
			array('7604596136','Web OP (MHouse)'),
			array('8773453217','Web OP (MHouse)'),
			array('4044607101','Web OP (Midtown)'),
			array('8885262640','Web OP (Midtown)'),
			array('6153706880','Web OP (Nashville)'),
			array('8558086208','Web OP (Nashville)'),
			array('7705573547','Web OP (Roswell)'),
			array('8558086204','Web OP (Roswell)'),
			array('8777141317','Web OP (Roswell)'),
			array('6193211575','Web OP (San Diego)'),
			array('7604596139','Web OP (San Diego)'),
			array('4152931680','Web OP (SanFran)'),
			array('4152931698','Web OP (SanFran)'),
			array('4248327368','Web OP (SantaMonic)'),
			array('4244655066','Web OP (SantaMonic)'),
			array('8558086207','Web OP (SantaMonic)'),
			array('7062004004','Web OP (T-Colmbs)'),
			array('7063413600','Web OP (T-Colmbs)'),
			array('4042701023','Web OP (T-Dunwdy)'),
			array('4049522500','Web OP (T-Dunwdy)'),
			array('8773453360','Jacy Brucks (Events)'),
			array('8558086210','_Events (Jacy)'),
			array('8773453361','Web The Oaks (Vets)'),
			array('8669720317','Web Sober [Pool:Network]')
		);
		return $old_numbers;
	}
// END OLD NUMBERS
//////////


//////////
// SMS SHORTCODE
	function frn_sms($atts, $content="", $sc=""){
		$defaults = array(
			'action' => 'Phone Clicks [SMS]'
		);
		$atts=wp_parse_args($atts,$defaults); 
		return frn_phone($atts, $content, $sc);
	}
// END SMS SHORTCODE
//////////




//////////////////////////////////
// DIALOGTECH SCRIPT/OPTIONS   //
//////////////////////////////////

//This script is added into the section where the wp_footer function is added in the PHP. 
//It's unlikely it's right before the /BODY tag. Dax isn't sure how that'll affect DT's find and replace of the phone numbers and Analytics reporting.
//The action that adds this to the footer is at the very bottom of this page with the rest of them
add_action('wp_footer', 'frn_dialogtech', 101);
function frn_dialogtech() {
	if(!is_admin()) {
		$phones = frn_phone_old_to_new();
		$activate=frn_dialogtech_activate($phones);

		if($activate=="yes") { 
			if(!isset( $phones['dialogtech_id'] ))  $phones['dialogtech_id']="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
			?>
		
	<!-- #####
	DialogTech Service
	##### -->
	<script type="text/javascript">
        var _stk = "<?php echo $phones['dialogtech_id']; ?>";
        (function(){
            var a=document, b=a.createElement("script"); b.type="text/javascript";
            b.async=!0; b.src=('https:'==document.location.protocol ? 'https://' :
            'http://') + 'd31y97ze264gaa.cloudfront.net/assets/st/js/st.js';
            a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
        })();
    </script>
    <!-- #####
	END DialogTech Service
	##### -->



	<?php }
	}
}
function frn_dialogtech_activate($phones=""){
	if(!is_array($phones)) $phones = frn_phone_old_to_new();
	if(!isset( $phones['tracking'] ))		$phones_tracking="";
		else $phones_tracking=$phones['tracking'];
	if(!isset( $phones['dialogtech'] )) 	$phones_dt=""; //old approach
		else $phones_dt=$phones['dialogtech'];
	if(!isset( $phones['dialogtech_id'] ))  $phones_dt_id="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
		else $phones_dt_id=$phones['dialogtech_id'];
	if($phones_dt!=="" || $phones_tracking=="dt") {
		return "yes";
	}
	if(!isset($phones['infinity'])) $inf_options="";
		else $inf_options = $phones['infinity'];
	if(!isset($inf_options['activate'])) $inf_activate="";
		else $inf_activate = $inf_options['activate'];
	global $inf_main;

	if($phones_tracking=="dt-ads") {
		//The st-t var must be present in URL or the cookie value stored
		//Activate DT whether or not Infinity is activated
		$tag =frn_phone_get_url_tag();
		if($tag!=="") {
			if(!isset($tag['tag'])) $tag_tag="";
				else $tag_tag=$tag['tag'];
			if(!isset($tag['value'])) $tag_value="";
				else $tag_value=$tag['value'];
			if($tag_tag=="st-t" && $tag_value!=="cancel") {
				if($inf_activate=="all") {
					$inf_main['activate']="js"; 
					//changes "all" to "js" to deactivate phone number replacement, but keep Infinity's analytics alive AND DT JS
					//FYI: although DT has a cookie option, we need to keep Inf deactivated for the visitor for a similar timeframe
				}
				//echo "URL tag activated DT and turned off Infinity replacement.<br />";
				return "yes"; //reminder: DT has it's own cookie system for url tags unlike infinity
			}
		}
		elseif(function_exists('frn_infinity_get_cookie')) {
			$cookie=frn_infinity_get_cookie();
			if($cookie!=="") {
				//echo "<br />URL tag: ".$tag['tag'];
				//echo "<br />Cookie result for DT activate: ";
				//if($cookie!=="") print_r($cookie);
				//else echo "not set";
				
				if(!isset($cookie['pool_type'])) $cookie_pool="";
					else $cookie_pool=$cookie['pool_type'];
				if($cookie_pool=="st-t" && $cookie_pool!=="cancel") {
					if($inf_activate=="all") {
						$inf_main['activate']="js"; 
						//changes "all" to "js" to deactivate phone number replacement, but keep Infinity's analytics alive AND DT JS
						//FYI: although DT has a cookie option, we need to keep Inf deactivated for the visitor for a similar timeframe
					}
					//echo "Cookie activated DT and turned off Infinity replacement.<br />";
					return "yes"; //reminder: DT has it's own cookie system for url tags unlike infinity
				}
			}
		}
	}
	//$cookie=frn_infinity_get_cookie();
	//echo "DialogTech not activated. ".$cookie['pool_type']."<br />";
	return "no";
}




/////////
// Phone JAVASCRIPT_SCAN Trigger
	/* Disabled 11/24/18, Never Used
	// Javascript phone detection feature only
	// Was initial version until realizing it slowed page load on the browser side
	// Tested in June 2017 and it wasn't working. Helps when phone numbers are in PHP locations that can't be changed or in fields where shortcodes aren't possible.
	//add_action('wp_footer', 'frn_phone_js_scan', 3); //3 to make sure the script is added below chartbeat, etc.
	function frn_phone_js_scan(){
	// This script runs for every page load.
	// Used to add phones2links.js to footer and see if entire page autoscan is turned on for JS file to scan the page for phone number formats and turn them into links using JS.
	global $frn_mobile;
	if($frn_mobile) {
		$frn_phone_ftr=""; $phone_linking="";

		//Turn on auto scanning
		$options_phone = get_option('site_phone_number');
		if(isset($options_phone['site_phone'])) $site_phone=$options_phone['site_phone'];
			else $site_phone="";

		//phone_linking = processing method
		if(isset($options_phone['phone_linking'])) $phone_linking=$options_phone['phone_linking'];
		$options_ua = get_option('site_head_code');
		$ga_ua = "_ua";
		if(isset($options_ua['frn_ga_ua'])) {
			if($options_ua['frn_ga_ua']!=="Activate") $ga_ua = ""; //Used to refer to other JS files that have the latest GA event and social tracking codes
		}
		
		//This feature was disabled due to the JavaScript replacing previously changed numbers in the sequence. So, A will never be an option.
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan=="A") {
			//if to scan all of the page, set a global JS variable that will turn on the autoscan feature in our normal phones2links.js
			//See if content ID block is set
			if(isset($options_phone['js_content_id'])) $js_content_id=trim($options_phone['js_content_id']);
			if($js_content_id!=="") $js_content_id = ' js_content_id="'.trim($options_phone['js_content_id']).'";';
			
			$frn_phone_ftr .=  '
	<script type="text/javascript">frn_phone_autoscan="yes";'.$js_content_id.'</script>'; 
	
		}
		
		//Checks if JS turned on manually or phone number is added via JavaScript
		if($auto_scan=="A" || $phone_linking=="JS" ) { //removed || strpos($site_phone,"script")!==false from this IF statement since it's no longer use by DialogTech.
		$frn_phone_ftr .=  '
	<script async=\'async\' type="text/javascript" src="'.plugin_dir_url( __FILE__ ).'part_phones2links'.$ga_ua.'.js"></script>
		';
		}
		
		echo $frn_phone_ftr;
	}
	}
*/

?>